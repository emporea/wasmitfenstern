﻿Public Class Teil

    Private wkz_daten As New List(Of Wkz)
    Private teil_daten As New List(Of String)
    Public Sub BlockItems(bez As String, blo As Boolean)
        For Each a In wkz_daten.Where(Function(i) i.Bezeichnung = bez).ToList
            a.Enabled = blo
        Next
    End Sub
    Public Sub ChangeIcon(bez As String, icon As String)
        For Each a In wkz_daten.Where(Function(i) i.Bezeichnung = bez).ToList
            a.Icon = icon
        Next
    End Sub
    Public Sub ChangeDraw(bez As String, pentmp As Pen, brushtmp As SolidBrush)
        For Each a In wkz_daten.Where(Function(i) i.Bezeichnung = bez).ToList
            a.drwPen = pentmp
            a.drwBrush = brushtmp
        Next
    End Sub
    Public Sub CheckChanged()
        For Each a In wkz_daten
            If a.Enabled = False Or a.Icon IsNot "" Then
                a.Changed = True
            Else
                a.Changed = False
            End If
        Next
    End Sub

    Public Function GetChangedWkz() As List(Of Wkz)
        Return wkz_daten.GroupBy(Function(x) x.Bezeichnung).Select(Function(x) x.First).ToList.Where(Function(i) i.Changed = True).ToList
    End Function
    Public Function GetWkzItems(tmp As String) As List(Of Wkz)
        Return wkz_daten.Where(Function(i) i.Bezeichnung = tmp And i.Enabled = True).ToList
    End Function
    Public Function GetGroupedItems() As List(Of Wkz)
        Return wkz_daten.GroupBy(Function(x) x.Bezeichnung).Select(Function(x) x.First).ToList
    End Function
    Public Function GetDrawItems() As List(Of Wkz)
        Return wkz_daten.Where(Function(i) i.Enabled = True).ToList
    End Function
    Public Sub SortedXPos()
        wkz_daten.Sort(Function(x, y) x.XPos.CompareTo(y.XPos))
        wkz_daten.Reverse()
    End Sub
    Public Sub New(tmp As List(Of String))
        For Each a In tmp
            teil_daten.Add(a)
        Next
    End Sub
    Public Function GetDaten(index As Integer) As String
        Return teil_daten(index)
    End Function

    Public Sub NewWkz(bez_str As String, xpos_s As Double, ypos_s As Double, bn_s As String, lng As String)
        wkz_daten.Add(New Wkz(bez_str, xpos_s, ypos_s, bn_s, lng))
    End Sub
End Class
