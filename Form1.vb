﻿Imports System.Xml
Imports System.IO

Public Class Form1
    'Declaration of Variables
    Dim XPadding As Integer = 140
    Dim YFrame As Integer = 30
    Dim IconSize As Integer = 30
    Dim TextDistance As Integer = 14
    Dim YPadding As Integer = 40 + TextDistance * 5 + YFrame
    Dim YScaling As Single = 0.75
    Dim YCon As Single = 0

    ReadOnly Xml_attributes() As String = {"Position", "Laenge", "AnGehrung", "AbGehrung", "Bezeichnung", "Kommission", "Stahllaenge", "StahlNr", "Barcode", "TeileNr", "Stahleinschubtiefe"}
    ReadOnly version As String = "V2.2"
    ReadOnly prgrm_n As String = "WasMitFenstern"
    ReadOnly conf_filename As String = "config.cfg"
    Dim text_perpart = "Einstellungen Pro Datei"
    Dim text_perglobal = "Einstellungen Global"

    Dim nmbDraws As Integer = 0
    Dim ConfIsValid As Boolean = False
    Dim MaybeSave As Boolean = False
    Dim filename As String = ""
    Dim FileIsOpen As Boolean = False
    Dim ShowAll As Boolean = False
    Dim old_Draw_wkz As String = ""
    Dim old_current As Integer = -1

    Dim XMlConserve As New List(Of String)
    Dim current As Integer = 0
    Dim Teile As New List(Of Teil)
    Dim Loaded_Wkz As New List(Of Wkz)
    Dim Config_Wkz As New List(Of Wkz)
    Dim Labels As New List(Of Label)

    Dim WithEvents F2 As New Form2
    Dim WithEvents F3 As New Form3
    Dim WithEvents TrackBarIcon As TrackBar = F3.TrackBar1
    Dim WithEvents TrackBarXPad As TrackBar = F3.TrackBar2
    Dim WithEvents TrackBarYPad As TrackBar = F3.TrackBar3
    Dim WithEvents TrackBarYcon As TrackBar = F3.TrackBar4
    Dim WithEvents TreeViewF2 As TreeView = F2.TreeView1
    Dim WithEvents ListViewF2 As ListView = F2.ListView1
    Dim WithEvents ContextStripF2 As ContextMenuStrip = F2.ContextMenuStrip1
    Dim WithEvents MenuItemF2 As ToolStripMenuItem = F2.ShowAllToolStripMenuItem
    Dim WithEvents MenuItem2F2 As ToolStripMenuItem = F2.IconAuswählenToolStripMenuItem
    Dim WithEvents MenuItem3F2 As ToolStripMenuItem = F2.LöscheIconToolStripMenuItem

    <System.STAThread()> Public Shared Sub Main()
        Application.EnableVisualStyles()
        'Application.SetCompatibleTextRenderingDefault(False)

        Application.Run(New Form1)
    End Sub

    Private Sub TreeDraw()
        TreeViewF2.Nodes.Clear()

        Dim MyImages As New ImageList()
        Dim wkzlist As New List(Of Wkz)
        Dim iter As Integer = 0

        If ShowAll = True Then                                                                 'Do This if only Show  WKZ Per Part
            wkzlist.AddRange(Loaded_Wkz)
            wkzlist.AddRange(Config_Wkz)
        Else                                                                                    'Show All Wkz of all Parts in the File
            wkzlist.AddRange(Loaded_Wkz)
        End If

        For Each treewkz In wkzlist
            Dim newNode As New TreeNode
            MyImages.ImageSize = New Size(30, 30)
            If treewkz.Icon IsNot "" Then MyImages.Images.Add(New Bitmap(treewkz.Icon)) Else MyImages.Images.Add(New Bitmap(30, 30))
            newNode.Text = "W=" & treewkz.Bezeichnung
            newNode.ImageIndex = iter
            newNode.SelectedImageIndex = iter
            newNode.Checked = treewkz.Enabled



            TreeViewF2.Nodes.Add(newNode)

            iter += 1
        Next
        TreeViewF2.ImageList = MyImages

    End Sub

    Private Sub ListDraw() 'Refresh TreeView and TreeList with Content
        If FileIsOpen Then

            ListViewF2.Items.Clear()
            ListViewF2.Columns.Clear()

            Dim arr(2) As String
            Dim tmp_int As Double = 0


            ListViewF2.Columns.Add("XPos", 80, HorizontalAlignment.Center)
            ListViewF2.Columns.Add("Abstand", 80, HorizontalAlignment.Center)


            For Each listwkz In Teile(current).GetGroupedItems

                Dim poslist = Teile(current).GetWkzItems(listwkz.Bezeichnung)
                Dim newGroup As New ListViewGroup("W=" & listwkz.Bezeichnung & "  |  Summe=" & poslist.Count)
                Dim newItem As New ListViewItem

                ListViewF2.Groups.Add(newGroup)


                For Each pos In poslist

                    If pos IsNot poslist.First Then
                        arr(0) = ""
                        arr(1) = Math.Abs(pos.XPos - tmp_int) & " mm"
                        newItem = New ListViewItem(arr, "", newGroup)
                        ListViewF2.Items.Add(newItem)
                    End If

                    arr(0) = pos.XPos & " mm"
                    arr(1) = ""

                    newItem = New ListViewItem(arr, "", newGroup)
                    ListViewF2.Items.Add(newItem)

                    tmp_int = pos.XPos

                Next
            Next


        End If

    End Sub

    Private Sub SaveConfig()
        If ConfIsValid Then
            Try
                Dim tmpwkz As New List(Of Wkz)
                tmpwkz.AddRange(Loaded_Wkz)
                tmpwkz.AddRange(Config_Wkz)

                Dim w As New XmlTextWriter(conf_filename, System.Text.Encoding.UTF8)

                w.WriteStartDocument(True)
                w.Formatting = Formatting.Indented
                w.WriteStartElement("Save")
                w.Indentation = 2
                w.WriteStartElement("Config")
                w.WriteAttributeString("IconSize", TrackBarIcon.Value.ToString)
                w.WriteAttributeString("XPadding", TrackBarXPad.Value.ToString)
                w.WriteAttributeString("YScaling", TrackBarYPad.Value.ToString)
                w.WriteAttributeString("YCon", TrackBarYcon.Value.ToString)
                w.WriteEndElement()

                For Each a In tmpwkz
                    If a.Changed Then
                        w.WriteStartElement("ProfilBearb")
                        w.WriteAttributeString("Bezeichnung", a.Bezeichnung)
                        w.WriteAttributeString("Enabled", a.Enabled)
                        w.WriteAttributeString("Icon", a.Icon)
                        w.WriteEndElement()
                    End If
                Next
                w.WriteEndDocument()
                w.Close()
                MaybeSave = False
                Draw()
            Catch
                MessageBox.Show("Speichern fehlgeschlagen!")
            End Try
        Else
            MessageBox.Show("Config.xml Fehlerhaft. Datei löschen oder reparieren!")
        End If


    End Sub

    Private Function WkzInCurrentFile(bez As String) As Boolean
        Dim booltmp As Boolean = False
        For Each a In Teile
            For Each b In a.GetGroupedItems
                If bez = b.Bezeichnung Then booltmp = True
            Next
        Next
        Return booltmp
    End Function


    Private Sub LoadConfig()

        If File.Exists(conf_filename) Then
            Dim tmp_int As Integer = 0

            Dim l As XmlReader = XmlReader.Create(conf_filename)
            If XMlConserve IsNot Nothing Then XMlConserve.Clear()
            Try
                ConfIsValid = False


                Do While l.Read()

                    If l.NodeType = XmlNodeType.Element AndAlso l.Name = "Config" Then
                        IconSize = CInt(l.GetAttribute("IconSize")) * 5 : TrackBarIcon.Value = IconSize / 5
                        XPadding = CInt(l.GetAttribute("XPadding")) * 10 : TrackBarXPad.Value = XPadding / 10
                        YScaling = CInt(l.GetAttribute("YScaling")) / 22 + 0.1 : TrackBarYPad.Value = CInt((YScaling - 0.1) * 22)
                        YCon = CInt(l.GetAttribute("YCon")) / 40 : TrackBarYcon.Value = YCon * 40
                    End If

                    If l.NodeType = XmlNodeType.Element AndAlso l.Name = "ProfilBearb" Then
                        Dim tmpbez As String = l.GetAttribute("Bezeichnung")
                        Dim tmpicon As String = l.GetAttribute("Icon")
                        Dim tmpenabled As String = l.GetAttribute("Enabled")

                        If WkzInCurrentFile(tmpbez) Then
                            For Each a In Loaded_Wkz
                                If a.Bezeichnung = tmpbez Then
                                    a.Icon = tmpicon
                                    a.Enabled = tmpenabled
                                End If
                            Next
                        Else
                            Dim tmpwkz As New Wkz(tmpbez, 0, 0, "", tmpicon, tmpenabled)
                            Config_Wkz.Add(tmpwkz)
                        End If
                    End If
                Loop
                ConfIsValid = True
            Catch ex As Exception
                '    ' Show the exception's message.
                ' MessageBox.Show(ex.Message)
                MessageBox.Show("Config.xml Fehlerhaft. Datei löschen oder reparieren!")
            End Try
            l.Close()
        Else
            ConfIsValid = True
        End If

    End Sub



    Sub update_teil()
        For Each a In Teile
            For Each b In Loaded_Wkz
                a.ChangeIcon(b.Bezeichnung, b.Icon)
                a.BlockItems(b.Bezeichnung, b.Enabled)
                a.ChangeDraw(b.Bezeichnung, b.drwPen, b.drwBrush)
            Next
        Next

        For Each a In Loaded_Wkz
            If a.Enabled = False Or a.Icon IsNot "" Then
                a.Changed = True
            Else
                a.Changed = False
            End If
        Next
        For Each a In Config_Wkz
            If a.Enabled = False Or a.Icon IsNot "" Then
                a.Changed = True
            Else
                a.Changed = False
            End If
        Next



    End Sub

    Private Sub Draw(Optional Refresh As Boolean = False)
        If FileIsOpen Then
            Try
                'nmbDraws += 1
                Dim SizeWindowX As Integer = PictureBox1.Width - 2 * XPadding
                Dim SizeWindowY As Integer = PictureBox1.Height - 2 * YPadding
                Dim YDiff_tmp As Single = 0

                Dim blackPen As Pen = New Pen(Color.Black, 1)
                Dim ImageChache As New Bitmap(PictureBox1.Width, PictureBox1.Height)
                Dim Graph As Graphics = Graphics.FromImage(ImageChache)
                Dim fatPen As Pen = New Pen(Color.Black, 2)
                Dim drawBrush As SolidBrush = New SolidBrush(Color.Black)
                Dim drawFont As Font = New Font("Times New Roman", 10)
                Dim drawFormat As StringFormat = New StringFormat()
                Dim FrameRect As Rectangle = New Rectangle(XPadding, YPadding - YFrame, SizeWindowX, YFrame)
                Dim IconRect As Rectangle

                drawFormat.Alignment = StringAlignment.Center
                Graph.Clear(Me.BackColor)


                Graph.DrawRectangle(blackPen, FrameRect)

                SpeichernToolStripMenuItem.Enabled = MaybeSave
                update_teil()
                'Graph.DrawString("NmbDraws: " & nmbDraws, drawFont, drawBrush, 12, 12)
                For i As Integer = 0 To Xml_attributes.Length - 2
                    Dim tmp_str As String


                    If Xml_attributes(i) = "Laenge" Or Xml_attributes(i) = "Stahllaenge" AndAlso Teile(current).GetDaten(i) IsNot " / " Then
                        tmp_str = Teile(current).GetDaten(i) & " mm"
                        If Xml_attributes(i) = "Stahllaenge" And Teile(current).GetDaten(10) IsNot " / " Then
                            tmp_str = tmp_str & "   (Einschubtiefe " & Teile(current).GetDaten(10) & " mm)"
                        End If
                    ElseIf Xml_attributes(i) = "AnGehrung" Or Xml_attributes(i) = "AbGehrung" Then
                        tmp_str = Teile(current).GetDaten(i).Substring(0, 2) & "°"
                    Else
                        tmp_str = Teile(current).GetDaten(i)
                    End If
                    If i < 5 Then
                        Graph.DrawString(Xml_attributes(i), drawFont, drawBrush, XPadding, 30 + (i * TextDistance))
                        Graph.DrawString(tmp_str, drawFont, drawBrush, XPadding + 0.2 * SizeWindowX, 30 + (i * TextDistance))
                    Else
                        Graph.DrawString(Xml_attributes(i), drawFont, drawBrush, XPadding + (SizeWindowX / 2), 30 + ((i - 5) * TextDistance))
                        Graph.DrawString(tmp_str, drawFont, drawBrush, XPadding + 0.7 * SizeWindowX, 30 + ((i - 5) * TextDistance))
                    End If
                Next

                For Each i In Teile(current).GetDrawItems()

                    Dim XPos_tmp As Single = (i.XPos * SizeWindowX) / Teile(current).GetDaten(1) + XPadding
                    Dim Ypos_tmp As Single = YPadding + 50 + YDiff_tmp
                    Dim bez_tmp As String = i.Bezeichnung
                    Dim bnr_tmp As String = i.BNr
                    Dim ypos_str As String = ""
                    Dim lng_str As String = ""
                    If i.YPos > 0 Then ypos_str = " y=" & i.YPos
                    If i.Laenge > 0 Then lng_str = " L=" & i.Laenge
                    Dim xpos_str As String = " x=" & i.XPos
                    IconRect = New Rectangle(XPos_tmp - IconSize / 2, 0.5 * (Ypos_tmp + YPadding - IconSize) + YCon * YDiff_tmp, IconSize, IconSize)

                    Graph.DrawLines(i.drwPen, {New Point(XPos_tmp, YPadding), New Point(XPos_tmp, Ypos_tmp), New Point(XPos_tmp + 10, Ypos_tmp)})
                    Graph.DrawString(bez_tmp & "(" & bnr_tmp & ")" & xpos_str & ypos_str & lng_str, drawFont, i.drwBrush, XPos_tmp + 12, Ypos_tmp - 5)
                    If i.Icon IsNot "" Then Graph.DrawImage(New Bitmap(i.Icon), IconRect) Else Graph.DrawRectangle(i.drwPen, IconRect)

                    YDiff_tmp += ((PictureBox1.Height - (YPadding + 50)) * YScaling) / Teile(current).GetDrawItems().Count - 1
                Next

                YDiff_tmp = 0
                If Not old_current = current Then


                    If Labels IsNot Nothing Then
                        For Each i In Labels
                            PictureBox1.Controls.Remove(i)
                        Next
                        Labels.Clear()
                    End If

                    Dim L As Label
                    Dim tmp_count As Integer = 0
                    For Each i In Teile(current).GetGroupedItems()

                        L = New Label

                        L.Name = "Label" & i.Bezeichnung
                        L.Text = "W=" & i.Bezeichnung & " (" & Teile(current).GetWkzItems(i.Bezeichnung).Count & ")"
                        'L.Left = 100
                        'L.Top = 100
                        L.Location = New Point(PictureBox1.Width - 150, PictureBox1.Height - 200 + YDiff_tmp)
                        L.Height = TextDistance + 3
                        L.Width = 100
                        L.BackColor = Me.BackColor
                        L.ForeColor = Color.Red
                        AddHandler L.MouseEnter, AddressOf Me.Label_Enter
                        AddHandler L.MouseLeave, AddressOf Me.Label_Leave
                        Labels.Add(L)

                        PictureBox1.Controls.Add(Labels.ElementAt(tmp_count))
                        'Graph.DrawString("W=" & i.Bezeichnung & " (" & Teile(current).GetWkzItems(i.Bezeichnung).Count & ")", drawFont, redBrush, New Point(PictureBox1.Width - 150, PictureBox1.Height - 200 + YDiff_tmp))
                        YDiff_tmp += TextDistance + 3
                        tmp_count += 1
                    Next
                    old_current = current
                Else
                    For Each i In Labels
                        i.Location = New Point(PictureBox1.Width - 150, PictureBox1.Height - 200 + YDiff_tmp)
                        YDiff_tmp += TextDistance + 3
                    Next
                End If

                PictureBox1.Image = ImageChache

            Catch
                MessageBox.Show("Fehler beim Laden der Resourcen")
            End Try

        End If

    End Sub


    Private Sub Label_Leave(sender As Object, e As EventArgs)
        Dim L As New Label
        L = DirectCast(sender, Label)
        L.BackColor = Me.BackColor
        Dim tmpstr As String = L.Name.Substring(5)
        'If Not tmpstr = old_Draw_wkz Then
        For Each i_teil In Loaded_Wkz
                If i_teil.Bezeichnung = tmpstr Then
                i_teil.drwBrush = New SolidBrush(Color.Black)
                i_teil.drwPen = New Pen(Color.Black, 1)
            End If
            Next
        old_Draw_wkz = ""
        Draw()
        'End If
        'MessageBox.Show("Leave???")
    End Sub
    Private Sub Label_Enter(sender As Object, e As EventArgs)
        Dim L As New Label
        L = DirectCast(sender, Label)
        L.BackColor = Color.Pink
        Dim tmpstr As String = L.Name.Substring(5)
        If Not tmpstr = old_Draw_wkz Then
            For Each i_teil In Loaded_Wkz
                If i_teil.Bezeichnung = tmpstr Then
                    i_teil.drwBrush = New SolidBrush(Color.Red)
                    i_teil.drwPen = New Pen(Color.Red, 2)
                End If
            Next
            old_Draw_wkz = tmpstr
            Draw()
        End If
    End Sub

    Private Sub LoadFile()
        Try
            Dim tmplist As New List(Of String)

            FileIsOpen = False
            MaybeSave = False
            current = 0
            old_current = -1

            If Teile IsNot Nothing Then
                Teile.Clear()
                Loaded_Wkz.Clear()
                Config_Wkz.Clear()
            End If

            Dim xr As XmlReader = XmlReader.Create(filename)
            Do While xr.Read()
                If xr.NodeType = XmlNodeType.Element AndAlso xr.Name = "Teiledaten" Then

                    If tmplist IsNot Nothing Then
                        tmplist.Clear()
                    End If
                    For i As Integer = 0 To Xml_attributes.Length - 1
                        If xr.GetAttribute(Xml_attributes(i)) Is Nothing Then
                            tmplist.Add(" / ")
                        Else
                            tmplist.Add(xr.GetAttribute(Xml_attributes(i)).ToString)
                        End If

                    Next
                    Teile.Add(New Teil(tmplist.ToList))
                End If
                If Teile IsNot Nothing Then
                    If xr.NodeType = XmlNodeType.Element AndAlso xr.Name = "ProfilBearb" Then
                        Teile(Teile.Count - 1).NewWkz(xr.GetAttribute("Wkz"), xr.GetAttribute("XPos"), xr.GetAttribute("YPos"), xr.GetAttribute("BNr"), xr.GetAttribute("Laenge"))
                    End If
                End If
            Loop

            xr.Close()

            If Teile.Count > 0 Then
                Me.Text = prgrm_n & " - " & filename
                FileIsOpen = True

                For Each i In Teile
                    i.SortedXPos()
                Next


                'SpeichernToolStripMenuItem.Enabled = FileIsOpen
                'old_current = current
                ToolsToolStripMenuItem.Enabled = FileIsOpen
                KonfigurationToolStripMenuItem.Enabled = FileIsOpen
                'Show All Wkz of all Parts in the File
                For Each a In Teile
                    Loaded_Wkz.AddRange(a.GetGroupedItems)
                    Loaded_Wkz = Loaded_Wkz.GroupBy(Function(x) x.Bezeichnung).Select(Function(x) x.First).ToList
                Next

                LoadConfig()
                Draw()
                TreeDraw()
                ListDraw()
            Else
                MessageBox.Show("XML-Datensatz leer oder Fehlerhaft")
            End If
        Catch
            MessageBox.Show("XML-Datensatz leer oder Fehlerhaft")
        End Try



    End Sub
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub
    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        OpenFileDialog1.Filter = "Xml Dateien (*.xml)|*.xml|Alle Datein (*.*)|*.*"
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.FilterIndex = 1
        OpenFileDialog1.RestoreDirectory = True
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()
        If result = Windows.Forms.DialogResult.OK Then
            filename = OpenFileDialog1.FileName
            LoadFile()
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        MessageBox.Show("Copyright Maurice Scholz 2020" & Environment.NewLine & Environment.NewLine & version)
    End Sub
    Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If FileIsOpen Then
            If e.KeyCode = Keys.Right Then
                If current < Teile.Count - 1 Then
                    current += 1
                    Draw()
                    ListDraw()
                    'TreeDraw()
                End If
            ElseIf e.KeyCode = Keys.Left Then
                If current > 0 Then
                    current -= 1
                    Draw()
                    ListDraw()
                    'TreeDraw()
                End If
            ElseIf e.KeyCode = Keys.Down Then


            End If

        End If
    End Sub
    Private Sub RefreshForm3()
        F3 = New Form3
        TrackBarIcon = F3.TrackBar1
        TrackBarIcon.Value = IconSize / 5

        TrackBarXPad = F3.TrackBar2
        TrackBarXPad.Value = XPadding / 10

        TrackBarYPad = F3.TrackBar3
        TrackBarYPad.Value = CInt((YScaling - 0.1) * 22)

        TrackBarYcon = F3.TrackBar4
        TrackBarYcon.Value = YCon * 40

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'LoadFile("C:\Users\Emporea\Downloads\017.xml")
        Me.AllowDrop = True

        Me.Text = prgrm_n
        SpeichernToolStripMenuItem.Enabled = FileIsOpen
        ToolsToolStripMenuItem.Enabled = FileIsOpen
        KonfigurationToolStripMenuItem.Enabled = FileIsOpen

        RefreshForm3()

        Dim s() As String = System.Environment.GetCommandLineArgs()
        If s.Length > 1 Then filename = s(1) : LoadFile()


    End Sub

    Private Sub ToolsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToolsToolStripMenuItem.Click
        If F2.Visible Then F2.Hide() Else RefreshForm2() : F2.Show()
        F2.Height = Me.Height / 2.5
        F2.Width = Me.Width / 2.5
        F2.Location = New Point(Me.Location.X + (Me.Width - F2.Width), Me.Location.Y + (Me.Height - F2.Height))
        If ShowAll Then MenuItemF2.Text = text_perpart : F2.Text = "Werkzeuge - " & text_perglobal Else MenuItemF2.Text = text_perglobal : F2.Text = "Werkzeuge - " & text_perpart
        Draw()
        ListDraw()
        TreeDraw()
    End Sub

    Private Sub TreeViewF2_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles TreeViewF2.AfterCheck
        For Each i_node In TreeViewF2.Nodes
            For Each i_teil In Loaded_Wkz
                If i_teil.Bezeichnung = i_node.Text.SubString(2) Then
                    i_teil.Enabled = i_node.Checked
                End If
            Next
            For Each i_teil In Config_Wkz
                If i_teil.Bezeichnung = i_node.Text.SubString(2) Then
                    i_teil.Enabled = i_node.Checked
                End If
            Next
        Next
        MaybeSave = True
        Draw()
        ListDraw()
    End Sub

    Private Sub TreeViewF2_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeViewF2.AfterSelect
        If TreeViewF2.SelectedNode IsNot Nothing Then MenuItem2F2.Enabled = True
    End Sub
    Private Sub IconAuswählenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuItem2F2.Click
        If TreeViewF2.SelectedNode IsNot Nothing Then
            OpenFileDialog1.Filter = "Bild Dateien (*.*)|*.*"
            OpenFileDialog1.FileName = ""
            OpenFileDialog1.FilterIndex = 1
            OpenFileDialog1.RestoreDirectory = False
            Dim result As DialogResult = OpenFileDialog1.ShowDialog()
            If result = Windows.Forms.DialogResult.OK Then
                Dim filetmp As String = OpenFileDialog1.FileName
                For Each i_teil In Loaded_Wkz
                    If i_teil.Bezeichnung = TreeViewF2.SelectedNode.Text.Substring(2) Then
                        i_teil.Icon = filetmp
                    End If
                Next
                For Each i_teil In Config_Wkz
                    If i_teil.Bezeichnung = TreeViewF2.SelectedNode.Text.Substring(2) Then
                        i_teil.Icon = filetmp
                    End If
                Next

            End If
            MaybeSave = True
            Draw()
            TreeDraw()
        End If
    End Sub
    Private Sub LöscheIconToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuItem3F2.Click
        If TreeViewF2.SelectedNode IsNot Nothing Then

            For Each i_teil In Loaded_Wkz
                If i_teil.Bezeichnung = TreeViewF2.SelectedNode.Text.Substring(2) Then
                    i_teil.Icon = ""
                End If
            Next
            For Each i_teil In Config_Wkz
                If i_teil.Bezeichnung = TreeViewF2.SelectedNode.Text.Substring(2) Then
                    i_teil.Icon = ""
                End If
            Next
            MaybeSave = True
            Draw()
            TreeDraw()
        End If
    End Sub
    Private Sub ShowAllToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuItemF2.Click
        ShowAll = Not ShowAll
        If ShowAll Then MenuItemF2.Text = text_perpart : F2.Text = "Werkzeuge - " & text_perglobal Else MenuItemF2.Text = text_perglobal : F2.Text = "Werkzeuge - " & text_perpart
        TreeDraw()
        Draw()
    End Sub
    Private Sub KonfigurationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KonfigurationToolStripMenuItem.Click
        If F3.Visible Then F3.Hide() Else RefreshForm3() : F3.Show()
    End Sub

    Private Sub TrackBarF3_Scroll(sender As Object, e As EventArgs) Handles TrackBarIcon.Scroll
        IconSize = TrackBarIcon.Value * 5
        MaybeSave = True
        Draw()
    End Sub
    Private Sub RefreshForm2()
        F2 = New Form2
        TreeViewF2 = F2.TreeView1
        ListViewF2 = F2.ListView1
        ContextStripF2 = F2.ContextMenuStrip1
        MenuItemF2 = F2.ShowAllToolStripMenuItem
        MenuItem2F2 = F2.IconAuswählenToolStripMenuItem
        MenuItem3F2 = F2.LöscheIconToolStripMenuItem

        ListViewF2.GridLines = True
    End Sub

    Private Sub TreeViewF2_NodeMouseClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles TreeViewF2.NodeMouseClick
        If e.Button = MouseButtons.Right Then TreeViewF2.SelectedNode = e.Node
    End Sub

    Private Sub Form1_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Draw()
    End Sub
    Private Sub TrackBarXPad_Scroll(sender As Object, e As EventArgs) Handles TrackBarXPad.Scroll
        XPadding = TrackBarXPad.Value * 10
        MaybeSave = True
        Draw()
    End Sub

    Private Sub TrackBarYPad_Scroll(sender As Object, e As EventArgs) Handles TrackBarYPad.Scroll
        YScaling = TrackBarYPad.Value / 22 + 0.1
        MaybeSave = True
        Draw()
    End Sub

    Private Sub SpeichernToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SpeichernToolStripMenuItem.Click
        SaveConfig()
    End Sub

    Private Sub TrackBarYcon_Scroll(sender As Object, e As EventArgs) Handles TrackBarYcon.Scroll
        YCon = TrackBarYcon.Value / 40
        MaybeSave = True
        Draw()
    End Sub

    Private Sub Form1_DragDrop(sender As Object, e As DragEventArgs) Handles Me.DragDrop
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop)
        filename = files.First
        LoadFile()
    End Sub

    Private Sub Form1_DragEnter(sender As Object, e As DragEventArgs) Handles Me.DragEnter
        e.Effect = DragDropEffects.Copy
    End Sub
End Class
