﻿Public Class Wkz
    Public Bezeichnung As String
    Public XPos As Double
    Public YPos As Double
    Public BNr As String
    Public Icon As String
    Public Laenge As String
    Public drwBrush As SolidBrush
    Public drwPen As Pen
    Public Enabled As Boolean
    Public Changed As Boolean
    Public Sub New(bez As String, x As Double, y As Double, bn As String, lng As String, Optional ico As String = "", Optional enable As Boolean = True)
        Bezeichnung = bez
        XPos = x
        YPos = y
        BNr = bn
        Laenge = lng
        Icon = ico
        Enabled = enable
        drwBrush = New SolidBrush(Color.Black)
        drwPen = New Pen(Color.Black, 1)
    End Sub
End Class
